import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalculatorResult } from '../models/CalculatorResult';
import { WedooStoreService } from '../services/wedoo-store/wedoo-store.service';



@Component({
  selector: 'app-wedoo-store',
  templateUrl: './wedoo-store.component.html',
  styleUrls: ['./wedoo-store.component.css']
})
export class WedooStoreComponent implements OnInit {

  @Output()
	amountOutput: EventEmitter<number> = new EventEmitter<number>();

  registerForm: FormGroup;
  submitted = false;
  regular:string = "^\\d+$";
  cardsEqual: number[];
  cardsFloor : number = null;
  cardsCeil : number = null;
  nextValue : number = null;
  amount : number;
  customerChoice : boolean = false;
  reasigned: boolean = false;
  calculatorResult: CalculatorResult;

  constructor(private wedooStoreService: WedooStoreService,private formBuilder: FormBuilder,) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      amount: ['', [Validators.required,Validators.pattern(this.regular),Validators.min(1)]]}, 
      );
  }
 
  get a() { 
    return this.registerForm.controls; 
  }

  onSubmit(shopId:number,newAmount:number) {
    this.reasigned = false;
    if (newAmount) {
      this.registerForm.get('amount').setValue(newAmount);
    }
    if (this.registerForm.invalid) {
      this.submitted = true;
      this.amount= null;
      this.cardsCeil = null;
      this.cardsEqual = null;
      this.cardsFloor = null;
    } else {
      
      this.submitted = false;
      //TODO : use the shop id in futur app
      this.wedooStoreService.getStoreCardsById(5, this.registerForm.get('amount').value).subscribe(
        (solutions :CalculatorResult) => {
          if (solutions) {
            this.calculatorResult = solutions;
              //equal
              if (this.calculatorResult.equal) {
                this.cardsEqual = this.calculatorResult.equal.cards;
              } else {
                //Case : if the value got no equal but is in range of cards
                if (this.calculatorResult.ceil && solutions.floor) {
                  this.nextValue = this.calculatorResult.ceil.value;
                  //TODO : make a modal it's better than boolean
                  this.customerChoice = true;
                } 
                //Case : if the value got no equal and it's out of range of cards
                if (this.calculatorResult.ceil && !this.calculatorResult.floor) {
                  this.changeAmount(this.calculatorResult.ceil.value);
                  this.reasigned = true;
                }
                if (!this.calculatorResult.ceil && this.calculatorResult.floor) {
                  this.changeAmount(this.calculatorResult.floor.value);
                  this.reasigned = true;
                }
              }
              //Minus and plus
              this.getMinusAndPlus(this.registerForm.get('amount').value) 
        }
       
      }, error => {
        //TODO : make an alert or toaster for error
        console.log(error);
      });
    }
      
  }

  //Method to change and update actuel amount
  changeAmount(newAmount : number) { 
    // if a new amount is chosen
    if (newAmount) {
      this.amount= newAmount;
      this.cardsCeil = null;
      this.cardsEqual = null;
      this.cardsFloor = null;
      this.onSubmit(null,newAmount);
    } 
    // Case if the customer choosed NO to the amount proposed
      else {
        this.amount= null;
        this.cardsCeil = null;
        this.cardsEqual = null;
        this.cardsFloor = null;
      }
    if (this.customerChoice = true) {
      this.customerChoice = !this.customerChoice;
    } 
  }

  //Method to calculate Minus and plus
  getMinusAndPlus(amount: number) {
    //Minus
    this.wedooStoreService.getStoreCardsById(5,(amount-1).toString()).subscribe(
      (solutionsMinus :CalculatorResult) => {
        if (solutionsMinus) {
          if (solutionsMinus.equal) {
            this.cardsFloor = solutionsMinus.equal.value;
          }
          if (!solutionsMinus.equal && solutionsMinus.floor) {
            this.cardsFloor = solutionsMinus.floor.value;
          }
        }
      }, error => {
        //TODO : make an alert or toaster for error
        console.log(error);
      });
    //Plus
    this.wedooStoreService.getStoreCardsById(5,(amount+1).toString()).subscribe(
      (solutionsPlus :CalculatorResult) => {
        if (solutionsPlus) {
          if (solutionsPlus.equal) {
            this.cardsCeil = solutionsPlus.equal.value;
          }
          if (!solutionsPlus.equal && solutionsPlus.ceil) {
            this.cardsCeil = solutionsPlus.ceil.value;
          }
        }
      }, error => {
        //TODO : make an alert or toaster for error
        console.log(error);
      });
  }

}
 

