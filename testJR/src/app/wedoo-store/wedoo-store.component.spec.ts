import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WedooStoreComponent } from './wedoo-store.component';

describe('WedooStoreComponent', () => {
  let component: WedooStoreComponent;
  let fixture: ComponentFixture<WedooStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WedooStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WedooStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
