import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { CalculatorResult } from '../../models/CalculatorResult';


@Injectable()
export class WedooStoreService {

  BASE_URL:string = "http://localhost:3000/shop/"


  constructor(private http: HttpClient) { }

  getStoreCardsById (storeID:number, desiredAmount:string): Observable<CalculatorResult> {
    let params = new HttpParams().set("amount", desiredAmount);
    return this.http
    .get<CalculatorResult>(this.BASE_URL+storeID+"/search-combination", {
      headers : {"Authorization": 'tokenTest123'},
      params : {"amount": desiredAmount}
    });
  }
}


