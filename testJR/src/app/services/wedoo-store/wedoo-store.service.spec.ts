import { TestBed, inject } from '@angular/core/testing';

import { WedooStoreService } from './wedoo-store.service';

describe('WedooStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WedooStoreService]
    });
  });

  it('should be created', inject([WedooStoreService], (service: WedooStoreService) => {
    expect(service).toBeTruthy();
  }));
});
