import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { WedooStoreComponent } from './wedoo-store/wedoo-store.component';
import { HttpClientModule } from '@angular/common/http';
import { WedooStoreService } from './services/wedoo-store/wedoo-store.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModal, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    WedooStoreComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    NgbModalModule,
    NgbModule.forRoot()
  ],
  providers: [WedooStoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
