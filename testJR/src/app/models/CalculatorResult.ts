export interface CalculatorResult { 
    equal : CalculatorComponentValue;
    ceil : CalculatorComponentValue;
    floor : CalculatorComponentValue;
}

export interface CalculatorComponentValue { 
    value : number;
    cards : number[];
}

